package xadkile.utility;

public class TestContent {
    String val;

    public TestContent(String val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return val;
    }
}
